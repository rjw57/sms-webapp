# Streaming Media Service Webapp

[![Documentation
Status](https://readthedocs.org/projects/uis-smswebapp/badge/?version=latest)](http://uis-smswebapp.readthedocs.io/en/latest/?badge=latest)
[![CircleCI](https://circleci.com/bb/uisautomation/sms-webapp.svg?style=svg)](https://circleci.com/bb/uisautomation/sms-webapp)

## Documentation

Automatically build documentation is viewable at
https://uis-smswebapp.readthedocs.org/.

## Running tests

```console
$ pip install tox
$ tox
```
